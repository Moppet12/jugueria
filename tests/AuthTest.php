<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    protected $name = 'John Doe';
    protected $email = 'john@example.com';
    protected $username = 'johndoe';
    protected $password = 'secret';

    /** @test */
    public function register_users()
    {
        $this->visit('/register')
            ->type($this->name, 'name')
            ->type($this->email, 'email')
            ->type($this->username, 'username')
            ->type($this->password, 'password')
            ->type($this->password, 'password_confirmation')
            ->press('Register')
            ->seeCredentials([
                'name' => $this->name,
                'email' => $this->email,
                'username' => $this->username,
                'password' => $this->password,
            ])
            ->seeIsAuthenticated()
            ->seePageIs('/admin');
    }

    /** @test */
    public function login_users()
    {
        $this->user([
            'username' => $this->username,
            'password' => $this->password,
        ]);

        $this->visit('/login')
            ->type($this->username, 'username')
            ->type($this->password, 'password')
            ->press('Acceder')
            ->seeCredentials([
                'username' => $this->username,
                'password' => $this->password,
            ])
            ->seeIsAuthenticated()
            ->seePageIs('/admin');
    }

    /** @test */
    public function logout_users()
    {
        $user = $this->user([
            'name' => $this->name,
        ]);

        $this->actingAs($user)
            ->visit('/logout')
            ->dontSeeIsAuthenticated()
            ->seePageIs('/');
    }
}
