<?php

namespace Jugueria;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';

    public function user(){
    	return $this->hasMany('Jugueria\User','id_sucursal');
    }
    
}
