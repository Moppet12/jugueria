<?php

namespace Jugueria;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
 	protected $table = 'categorias';

 	protected $hidden = [
 	];

 	public function platillo(){
 		return $this->hasMany('Jugueria\Platillo','id_categoria');
 	}
}
