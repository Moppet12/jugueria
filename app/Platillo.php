<?php

namespace Jugueria;

use Illuminate\Database\Eloquent\Model;

class Platillo extends Model
{
   	protected $table = 'platillos';

   	// Has

	public function ingrediente(){
    	return $this->belongsToMany('Jugueria\Ingrediente','ingredientes_platillos')
        ->withPivot('ingrediente_id');
    }

	// Belongs

	public function pedido(){
		return $this->belongsToMany('Jugueria\Ingrediente', 'pedidos_platillos');
	}

    public function categoria(){
    	return $this->belongsTo('Jugueria\Categoria','id_categoria');
    }

    public function inrediente(){
    	return $this->belongsTo('Jugueria\Ingrediente');
    }

}
