<?php

namespace Jugueria;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $table = 'pedidos';

    // Has

    	
    // BelongsTo

    public function platillo(){
   		return $this->belongsToMany('Jugueria\Platillo','pedidos_platillos')->withPivot('normal');
   	}
   	
    public function user(){
    	return $this->belongsTo('Jugueria\User','id_usuario');
    }

}
