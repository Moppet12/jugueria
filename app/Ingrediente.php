<?php

namespace Jugueria;

use Illuminate\Database\Eloquent\Model;

class Ingrediente extends Model
{
 	protected $table = 'ingredientes';

 	public function platillo(){
    	return $this->belongsToMany('Jugueria\Ingrediente','ingredientes_platillos')
    	->withPivot('platillo_id');
    }
}
