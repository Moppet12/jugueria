<?php

namespace Jugueria;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Has

    public function pedido(){
        return $this->hasMany('Jugueria\Pedido','id_usuario');
    }

    // Belongs
    
    public function sucursal(){
        return $this->belongsTo('Jugueria\Sucursal','id_sucursal');
    }

}
