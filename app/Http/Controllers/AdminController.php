<?php

namespace Jugueria\Http\Controllers;

use Illuminate\Support\Facades\App;

use Illuminate\Http\Request;

use Jugueria\Http\Requests;

use Carbon\Carbon;
use DB;
use Auth;
use Storage;

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\CapabilityProfiles\DefaultCapabilityProfile;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Mike42\Escpos\EscposImage;

use Jugueria\Categoria;
use Jugueria\Ingrediente;
use Jugueria\Ingrediente_Platillo;
use Jugueria\Pedido;
use Jugueria\Platillo;
use Jugueria\Pedido_Platillo;
use Jugueria\Sucursal;
use Jugueria\User;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function callPrinter($text){
        try {
            $connector = new WindowsPrintConnector("TM-20II");
            
            $printer = new Printer($connector);
            $img = EscposImage::load("logo.png", false);
            $printer -> bitImage($img, Printer::IMG_DOUBLE_WIDTH | Printer::IMG_DOUBLE_HEIGHT);
            $printer -> text($text);
            $printer -> cut();
            
            $printer -> close();
        } catch(Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }

    public function cajaIndex(Request $request){
        return view('admin.dashboard');
    }

    public function produccionIndex(Request $request){
        return view('admin.produccion');
    }

    public function userIndex(Request $request){
        if (Auth::user()->permiso != 1)
            return redirect('/login');

        return View('admin.admin');
    }

	// CRUD Categoria

    public function createCategoria(Request $request){
        $categoria = new Categoria;
        $categoria->nombre = $request['nombre'];
 		return ($categoria->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function readCategoria(Request $request){
        return ($categorias = Categoria::all()) ? $this->jsonSuccess($categorias) : $this->jsonError('');
    }

    public function updateCategoria(Request $request){
        $categoria = Categoria::find($request['id']);
        $categoria->nombre = $request['nombre'];
        return ($categoria->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function deleteCategoria(Request $request){
        return (Categoria::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');
    }

	// CRUD Ingrediente

    public function createIngrediente(Request $request){
        $ingrediente = new Ingrediente;
        $ingrediente->nombre = $request['nombre'];
        $ingrediente->estado = '1';
        return ($ingrediente->save()) ? $this->jsonSuccess('') : $this->jsonError('');   
    }

    public function readIngrediente(Request $request){
        if($x = DB::table('ingredientes_platillos')->select('ingrediente_id')->where('platillo_id',$request['id'])->get()){
            if($request['type'] == 1)
                return $x;
            $array = (array) $x;
            $ingredientes = array();
            foreach ($array as $key => $value) {
                $n = (array) $value;
                $ingredientes[$key] = Ingrediente::find($n['ingrediente_id']);
            }
                return view('admin.ingredientes',compact('ingredientes')); 
        }
        else{
            $ingredientes = array();
            return view('admin.ingredientes',compact('ingredientes')); 
        }    
    }


    public function updateIngrediente(Request $request){
        $ingrediente = Ingrediente::find($request['id']);
        $ingrediente->nombre = $request['nombre'];
        $ingrediente->estado = $request['estado'];
        return ($ingrediente->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function deleteIngrediente(Request $request){
        return (Ingrediente::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');        
    }

    public function readIngredientesPlatillo(Request $request){
        $platillo = Platillo::find(1)->ingrediente('ingrediente_id')->get();
    }

    public function readMoreIngredients(Request $request){
        // return ($ingredientes = Ingrediente::all()) ? : $this->jsonError('');
        $ingredientes = Ingrediente::all();
        foreach ($ingredientes as $key => $value) {
            // return $value['id'];
            if(count($request['except'])>0){
                foreach ($request['except'] as $k => $v) {
                    if($v == $value['id']){
                        unset($ingredientes[$key]);
                    }
                }
            }
        }
        return $ingredientes;
    }
	// CRUD Pedido

    public function testmodel(){
        $pedido = Pedido::find(20);
        return($pedido->platillo()->get());
    }

    public function createPedido(Request $request){
        $pedido_id = DB::table('pedidos')->orderBy('id', 'desc')->first()->id;
        $pedido = new Pedido;
        $ticketText = Carbon::now()."\nFolio: ". ++$pedido_id ."\n\n\n";
        $pedido->id_usuario = $request['id_usuario'];
        $pedido->tipo_pago = $request['pago_type'];
        $pedido->codigo_barras = random_int(0, 100);
        $pedido->estado = 1;
        foreach ($request['platillos'] as $platillo) {
            $ticketText .= $platillo['nombre_platillo'];
            $ticketText .= "\n                                   $".$platillo['precio']." \n";
            $pedido->normal = $platillo['normal'] == 'true' ? true : false;
            $pedido->costo += $platillo['precio'];
            $pedido->ingredientes_extra .= $platillo['ingredientes_extra'].',';
        }
        $ticketText .= "________________________________________________";
        $ticketText .= " \n                                   Total: $".$pedido->costo." \n             ¡Gracias por tu compra!\n";
        $pedido->save();

        foreach($request['platillos'] as $platillo){
            $pedido_platillo = new Pedido_Platillo;
            $pedido_platillo->pedido_id = $pedido->id;
            $pedido_platillo->platillo_id = $platillo['id_platillo'];
            $pedido_platillo->normal = $platillo['normal'] == 'true' ? true : false;;
            $pedido_platillo->save();
        }

        $notifyText = 'Pedido agregado';

        $pusher = App::make('pusher');

        // TODO: Get Pusher instance from service container
        
        $pedido['platillos'] = $pedido->platillo()->get();
        
        // TODO: The notification event data should have a property named 'text'

        $pusher->trigger('notifications',
                        'new-notification',
                        $pedido
                        );

        $this->callPrinter($ticketText);

        return $pedido;
        // TODO: On the 'notifications' channel trigger a 'new-notification' event


        return $this->jsonSuccess('');
    }

    public function readPedido(Request $request){
        return ($pedidos = 
            DB::table('pedidos')
            ->select(
                DB::raw('pedidos.id,
                pedidos.estado,
                pedidos.ingredientes_extra,
                platillos.nombre,
                pedidos.created_at,
                group_concat( platillos.nombre order by pedidos_platillos.id ) as "platillos",
                group_concat( pedidos_platillos.normal order by pedidos_platillos.id) as "normal"')
                )
            ->join('pedidos_platillos','pedidos.id', '=', 'pedidos_platillos.pedido_id')
            ->join('platillos', 'platillos.id','=', 'pedidos_platillos.platillo_id')
            ->where('pedidos.estado', '<', 2)
            ->groupBy('pedidos.id')
            ->orderBy('pedidos.id','desc')
            ->get()) ? 
            $this->jsonSuccess($pedidos) :
            $this->jsonError('');
    }

    public function updatePedido(Request $request){
        $pedido = Pedido::find($request['id']);
        $pedido->estado = $request['estado'];
        return ($pedido->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function deletePedido(Request $request){
        return (Pedido::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');
    }

	// CRUD Platillo

	public function createPlatillo(Request $request){
        $platillo = new Platillo;
        $platillo->nombre = $request['nombre'];
        $platillo->precio = $request['precio'];
        $platillo->id_categoria = $request['id_categoria'];
        return ($platillo->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function readPlatilloByCategoria(Request $request){
        return ($platillos = DB::table('platillos')->where('id_categoria', $request['id'])->get()) ? 
            $this->jsonSuccess($platillos) : 
            $this->jsonError('');
    }

    public function readPlatillo(Request $request){
        return 
        ($platillos = DB::table('platillos')
        ->join('categorias','platillos.id_categoria','=','categorias.id')
        ->select('platillos.*','categorias.nombre as nombre_categoria')
        ->get()) ? 
            $this->jsonSuccess($platillos) : 
            $this->jsonError('');
    }

    public function updatePlatillo(Request $request){
        $platillo = Platillo::find($request['id']);
        $platillo->nombre = $request['nombre'];
        $platillo->precio = $request['precio'];
        $platillo->id_categoria = $request['id_categoria'];
        return ($platillo->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function deletePlatillo(Request $request){
        return(Platillo::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');
    }

	// CRUD Sucursal

	public function createSucursal(Request $request){
        $sucursal = new Sucursal;
        $sucursal->nombre = $request['nombre'];
        $sucursal->ciudad = $request['ciudad'];
        $sucursal->id_responsable = $request['id_responsable'];
        return ($sucursal->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function readSucursal(Request $request){
        return ($sucursales = Sucursal::all()) ? $this->jsonSuccess($sucursales) : $this->jsonError('');
    }

    public function updateSucursal(Request $request){
        $sucursal = Sucursal::find($request['id']);
        $sucursal->nombre = $request['nombre'];
        $sucursal->ciudad = $request['ciudad'];
        $sucursal->id_responsable = $request['id_responsable'];
        return ($sucursal->save()) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function deleteSucursal(Request $request){
        return (Sucursal::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function createUser(Request $request){
        $user = new User();
        return $this->fillUser($user, $request);
    }

    public function readUsers(Request $request){
        return ($users = User::all()) ? $this->jsonSuccess($users) : $this->jsonError('');
    }

    public function editUser(Request $request){
        $user = User::find($request['id']);
        return $this->fillUser($user, $request);
    }

    private function fillUser($model, $request){
        $model->name = $request['name'];
        $model->email = $request['email'];
        $model->username = $request['username'];
        $model->password = bcrypt($request['password']);
        $model->permiso = ($request['permiso']) ? 1 : 0;
        $model->puesto = $request['puesto'];

        return ($model->save()) ?
            $this->jsonSuccess($model) :
            $this->jsonError();
    }

    public function deleteUser(Request $request){
        return (User::destroy($request['id'])) ? $this->jsonSuccess('') : $this->jsonError('');
    }

    public function getCaja(Request $request){
        return 
            ($pedido = DB::table('pedidos')->selectRaw('id')->orderBy('id', 'desc')->first()) ? 
                view('admin.caja', compact('pedido')) : 
                $this->jsonError('');
    }

    public function getCurrentOrder(Request $request){
        return 
            ($pedido = DB::table('pedidos')->selectRaw('id')->orderBy('id', 'desc')->first()) ? 
                $this->jsonSuccess($pedido) : 
                $this->jsonError('');
    }

    public function getDayOrders(){
        return ($orders = DB::table('pedidos')
            ->whereRaw('Date(created_at) = ?',[Carbon::now()->format('Y-m-d')])
            ->orderBy('id', 'desc')->get());
    }

    public function test(Request $request){
         
    }
    public function jsonSuccess($d){
        $array = [
            'r' => 'OK',
            'm' => 'Se ha completado la operación',
            'd' => $d
            ];
        if($d == '') unset($array['data']);
        return response()->json($array);
    }

    public function jsonError($d){
        $array = [
            'r' => 'ERROR',
            'm' => 'Ha ocurrido un error',
            'd' => $d
            ];
        if($d == '') unset($array['data']);
        return response()->json($array);
    }
}
