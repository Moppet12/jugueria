<?php

namespace Jugueria\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

use Jugueria\Http\Requests;
use Jugueria\Http\Controllers\AdminController as Admin;

class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        // $this->middleware('admin');
    }
    

    public function getDaySales(Request $request){
        $admin = new Admin();
        return ($sales = DB::table('pedidos')->selectRaw("
            sum(if(tipo_pago = 0, costo, 0)) as 'card', 
            sum(if(tipo_pago = 1, costo, 0)) as 'cash',
            sum(costo) as 'total'")->whereRaw('Date(created_at) = curdate()')->get()) ? 
            $admin->jsonSuccess($sales) : $admin->jsonError('');
    }

    public function getUsersPerformance(Request $request){
        return ($orders = DB::table('pedidos')
            ->select('costo')
            ->groupBy('<id_usuario></id_usuario>')
            ->get());
    }

    public function getWeeklySales(Request $request){
        return 
        DB::table('pedidos as p')
            ->selectRaw('Sum(p.costo) as sales, Day(p.created_at) as day, Dayname(p.created_at) as dayname, count(pp.id) as platillos')
            ->join('pedidos_platillos AS pp', 'p.created_at', '=', 'pp.created_at')
            ->where('p.created_at', '>=', '2017-07-01 00:00:00')
            ->where('p.created_at', '<=', Carbon::now()->endOfWeek())
            ->groupBy(DB::raw('Day(p.created_at)'))
            ->get();
    }

    public function getMonthlySales(Request $request){
        return 
        DB::table('pedidos as p')
            ->selectRaw('Sum(p.costo) as sales, Monthname(p.created_at) as monthname, count(pp.id) as platillos')
            ->join('pedidos_platillos AS pp', 'p.created_at', '=', 'pp.created_at')
            ->where('p.created_at', '>=', '2017-07-1 00:00:00')
            ->where('p.created_at', '<=', Carbon::now())
            ->groupBy(DB::raw('Month(p.created_at)'))
            ->get();
    }

    public function getMostSellProducts(Request $request){
        return
        DB::table('platillos as p')
            ->selectRaw('p.nombre, count(pp.platillo_id) as ventas')
            ->join('pedidos_platillos AS pp', 'p.id','=','pp.platillo_id')
            ->where('pp.created_at','>=',Carbon::now()->startOfDay())
            ->where('pp.created_at','<=',Carbon::now()->endOfDay())
            ->groupBy('pp.platillo_id')
            ->orderBy('ventas','desc')
            ->limit(7)
            ->get();
    }

}
