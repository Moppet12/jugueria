<?php

namespace Jugueria\Http\Controllers;

use Illuminate\Http\Request;

use Jugueria\Http\Requests;
use Jugueria\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;

class NotificationController extends Controller
{
    public function getIndex()
    {
        return view('notification');
    }

    public function postNotify(Request $request)
    {
        $notifyText = e($request->input('notify_text'));

        $pusher = App::make('pusher');

        // TODO: Get Pusher instance from service container

        $data = ['text' => $notifyText];

        // TODO: The notification event data should have a property named 'text'

        $pusher->trigger('notifications',
                        'new-notification',
                        $data
                        );

        // TODO: On the 'notifications' channel trigger a 'new-notification' event
    }
}
