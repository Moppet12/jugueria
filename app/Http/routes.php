<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//	Pusher stable tuto	
use Illuminate\Support\Facades\App;

Route::get('/bridge', function() {
    $pusher = App::make('pusher');

    $pusher->trigger( 'notificactions',
                      'new-notification', 
                      array('text' => 'This is a notificaction'));

    return view('notification');
});

Route::controller('notifications', 'NotificationController');

// \Pusher stable tuto	

Route::get('/', function () {
    return view('welcome');
});

// Auth

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/printer', 'AdminController@printer');

Route::get('/testmodel','AdminController@testmodel');
// \Auth


// Admin

Route::get('caja','AdminController@cajaIndex');
Route::get('produccion','AdminController@produccionIndex');
Route::get('admin','AdminController@userIndex');

Route::post('admin/create-categoria', 'AdminController@createCategoria');
Route::post('admin/read-categoria', 'AdminController@readCategoria');
Route::post('admin/update-categoria', 'AdminController@updateCategoria');
Route::post('admin/delete-categoria', 'AdminController@deleteCategoria');

Route::post('admin/create-ingrediente', 'AdminController@createIngrediente');
Route::post('admin/read-ingrediente', 'AdminController@readIngrediente');
Route::post('admin/update-ingrediente', 'AdminController@updateIngrediente');
Route::post('admin/delete-ingrediente', 'AdminController@deleteIngrediente');

Route::post('admin/read-ingredientes-platillo', 'AdminController@readIngredientesPlatillo');
Route::post('admin/read-more-ingredients', 'AdminController@readMoreIngredients');

Route::post('admin/create-pedido', 'AdminController@createPedido');
Route::post('admin/read-pedido', 'AdminController@readPedido');
Route::post('admin/update-pedido', 'AdminController@updatePedido');
Route::post('admin/delete-pedido', 'AdminController@deletePedido');

Route::post('admin/create-platillo', 'AdminController@createPlatillo');
Route::post('admin/read-platillo-by-categoria', 'AdminController@readPlatilloByCategoria');
Route::post('admin/read-platillo', 'AdminController@readPlatillo');
Route::post('admin/update-platillo', 'AdminController@updatePlatillo');
Route::post('admin/delete-platillo', 'AdminController@deletePlatillo');

Route::post('admin/create-sucursal', 'AdminController@createSucursal');
Route::post('admin/read-sucursal', 'AdminController@readSucursal');
Route::post('admin/update-sucursal', 'AdminController@updateSucursal');
Route::post('admin/delete-sucursal', 'AdminController@deleteSucursal');

Route::post('admin/create-user', 'AdminController@CreateUser');
Route::post('admin/read-users', 'AdminController@readUsers');
Route::post('admin/update-user', 'AdminController@editUser');
Route::post('admin/delete-user', 'AdminController@deleteUser');

Route::post('admin/get-caja','AdminController@getCaja');
Route::get('admin/get-current-order','AdminController@getCurrentOrder');

Route::get('admin/day-orders', 'AdminController@getDayOrders');

Route::get('admin/day-sales', 'SalesController@getDaySales');
Route::get('admin/weekly-sales', 'SalesController@getWeeklySales');
Route::get('admin/monthly-sales', 'SalesController@getMonthlySales');
Route::get('admin/most-sell-products', 'SalesController@getMostSellProducts');
Route::get('admin/users-performance', 'SalesController@getUsersPerformance');
// \Admin

Route::get('admin/test','AdminController@test');