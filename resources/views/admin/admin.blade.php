<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap3/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="components/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the csss
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="components/css/skins/_all-skins.min.css">

  <lin rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    .content-wrapper, .main-footer{
      margin: 0 !important;
    }
  </style>
</head>
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Jugueria</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span class="hidden-xs">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-footer">
                  <div class="pull-right">
                    <a href="logout" class="btn btn-default btn-flat">Cerrar Sesión</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Administración Juguería
          <small></small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">

        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#sells" role="tab"><i class="fa fa-line-chart"></i> Ventas</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#production" role="tab"><i class="fa fa-check-square-o"></i> Producción</a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#products" role="tab"><i class="fa fa-cart-plus"></i> Productos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#users" role="tab"><i class="fa fa-users"></i> Usuarios</a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="sells" role="tabpanel">
            <div class="row">
              <div class="col-md-6">
                <!-- AREA CHART -->
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ventas Semanales</h3>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                      <canvas id="areaChart" style="height:250px"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- DONUT CHART -->
                <div class="box box-danger">
                  <div class="box-header with-border">
                    <h3 class="box-title"> Ventas Diarias</h3>
                  </div>
                  <div class="box-body">
                    <div class="col-md-12" align="center">
                      <h3>Total</h3>
                      <h4 id="totalSales" class="text-center"></h4>
                    </div>
                    <div class="col-md-6">
                      <h3>Pagos en Efectivo</h3>
                      <h4 id="cashSales" class="text-center"></h4>
                    </div>
                    <div class="col-md-6">
                      <h3>Pagos con Tarjeta</h3>
                      <h4 id="cardSales" class="text-center"></h4>
                    </div>
                    <div class="chart">
                      <canvas id="barChart" style="height:230px"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->

              </div>
              <!-- /.col (LEFT) -->
              <div class="col-md-6">
                <!-- LINE CHART -->
                <div class="box box-info">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ventas Mensuales</h3>
                  </div>
                  <div class="box-body">
                    <div class="chart">
                      <canvas id="lineChart" style="height:250px"></canvas>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->

              </div>
              <!-- /.col (RIGHT) -->
            </div>
            <!-- /.row -->
          </div>


          <!-- <div class="tab-pane" id="production" role="tabpanel">

          </div> -->
          
          <!-- Products Panel-->

          <div class="tab-pane" id="products" role="tabpanel" style="min-height: 750px;">

            <div class="col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 id="productTitle" class="products">Registrar Productos</h3>
                  <div class="alert alert-warning" id="productEditAlert">
                    Detener Edición de Productos 
                    <button type="button" class="btn btn-primary btn-sm pull-right" onClick="stopEdit('Producto','product')">
                      <i class="fa fa-stop"></i>
                    </button>
                  </div>
                  <div class="box-body">
                    <div class="col-md-12">
                      <div class="panel panel-default">
                        <div class="panel-body">
                          <form class="form-horizontal" id="productForm" role="form" method="POST" name="form-product"
                          onsubmit="submitForm('productForm','create-platillo')">
                            {{ csrf_field() }}

                            <div class="row">
                              <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nombre</label>
                                <div class="col-md-8">
                                  <input id="name" type="text" class="form-control" name="nombre">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="form-group">
                                <label for="precio" class="col-md-4 control-label">Precio</label>
                                <div class="col-md-8">
                                  <input id="precio" type="number" class="form-control" name="precio">
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="form-group">
                                <label for="categoria" class="col-md-4 control-label">Categoría</label>
                                <div class="col-md-8">
                                  <select id="categoria" class="form-control" name="id_categoria">
                                  </select>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                  <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-cart-plus"></i> Registrar
                                  </button>
                                </div>
                              </div>
                            </div>
                            <div id="ProductsAlerts" class="alert"></div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <h3>Listado de Producto</h3>
                </div>
                <div class="box-body">
                  <table class="table table-striped table-bordered" id="productsList">
                    <thead>
                      <tr>
                        <th>Nombre</th>
                        <th>Precio</th>
                        <th>Categoría</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>

          <!-- Products Panel-->

          <!-- Users Panel -->
          <div class="tab-pane" id="users" role="tabpanel">
            <div class="container">
              <div class="row">

                <div class="col-md-6">
                  <div class="box box-primary">
                    <div class="box-header with-border">
                      <h3 class="box-title" id="userTitle" class="users">Registrar Usuario</h3>
                    </div>
                    <div class="alert alert-warning" id="userEditAlert">
                      Detener edición de Usuario
                      <button class="btn btn-primary btn-sm pull-right" onClick="stopEdit('Usuario', 'user')">
                        <i class="fa fa-stop"></i>
                      </button>
                    </div>
                    <div class="box-body">
                      <div class="col-md-12">
                        <div class="panel panel-default">
                          <div class="panel-body">
                            <form class="form-horizontal" id="userForm" role="form" method="POST" name="form-user"
                            onsubmit="submitForm('userForm','create-user')">
                              {{ csrf_field() }}

                              <div class="row">
                                <div class="form-group">
                                  <label for="name" class="col-md-4 control-label">Nombre</label>
                                  <div class="col-md-8">
                                    <input id="name" type="text" class="form-control" name="name">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <label for="email" class="col-md-4 control-label">Email</label>
                                  <div class="col-md-8">
                                    <input id="email" type="text" class="form-control" name="email">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <label for="username" class="col-md-4 control-label">Nombre de Usuario</label>

                                  <div class="col-md-8">
                                    <input id="username" type="text" class="form-control" name="username">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <label for="password" class="col-md-4 control-label">Contraseña</label>

                                  <div class="col-md-8">
                                    <input id="password" type="password" class="form-control" name="password">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <label for="admin" class="col-md-4 control-label">Administrador</label>

                                  <div class="col-md-8">
                                    <input id="admin" type="checkbox" name="permiso">
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <label for="puesto" class="col-md-4 control-label">Puesto</label>

                                  <div class="col-md-8">
                                    <select id="puesto" class="form-control" name="puesto">
                                      <option value="1">Administrador</option>
                                      <option value="2">Producción</option>
                                      <option value="3">Caja</option>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group">
                                  <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                      <i class="fa fa-btn fa-user"></i> Registrar
                                    </button>
                                  </div>
                                </div>
                              </div>
                              <div id="ProductsAlerts" class="alert"></div>
                            </form>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="box box-warning">
                    <div class="box-header with-border">
                      <h3 class="box-title">Listado de Usuarios</h3>
                    </div>

                    <div class="box-body">
                      <div class="col-md-12">
                        <table class="table table-striped table-bordered" id="usersList">
                          <thead>
                            <tr>
                              <th>Nombre</th>
                              <th>Usuario</th>
                              <th>Puesto</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /Users Panel -->


        </div>

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
    </footer>
  </div>
  <!-- ./wrapper -->

  <script src="components/js/jquery-3.1.0.min.js"></script>
  <script src="bootstrap3/js/bootstrap.min.js"></script>
  <!-- ChartJS 1.0.1 -->
  <script src="plugins/chartjs/Chart.min.js"></script>
  <script src="plugins/chartjs/ChartDemo.js"></script>
  <!-- FastClick -->
  <script src="plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="components/js/app.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="components/js/demo.js"></script>
  <!-- page script -->
  <script src="components/js/admin.js"></script>
  <!-- DataTables -->
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
</body>
</html>
