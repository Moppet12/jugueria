<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>La Juguería | Dashboard</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="components/css/style.css" rel="stylesheet">
</head>
<body>
	<header>
		<div class="container">
			<h1 class="text-md-center main-title">Elige la Categoría</h1>
			<div class="user">
				<div class="pull-md-right username">
					Hola {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
					<div class="btn-group">
						<button class="options" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-caret-down"></i>
						</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="logout"> Cerrar Sesión </a>
						</div>
					</div>
				</div>
				<div class="pull-md-right user-icon">
					<i class="fa fa-user"></i>
				</div>
				
			</div>
		</div>
	</header>
	<section class="container" id="dashboardBody">
		
	</section>
	<footer>
		<p class="sucursal pull-md-left">Sucursal Constitución Centro</p>
	</footer>

	<div class="modal fade notification-modal" id="modal1" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	    	<div class="insider">
	      		enviaste a caja: <br>
	      		<span class="standOut" id="product"></span>
	      		<button class="jugueria-button jugueria-button-sm btn-green" id="caja">Ir a caja</button>
	      		<button class="jugueria-button jugueria-button-sm btn-blue" id="ordenar">Ordenar Más</button>
	    	</div>
	    </div>
	  </div>
	</div>

	<div class="modal fade notification-modal" id="modal2" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	    	<div class="insider">
	      		
	    	</div>
	    </div>
	  </div>
	</div>

	<div class="modal fade notification-modal" id="modal3" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="insider">
	      		
	    	</div>
	    </div>
	  </div>
	</div>

	<div class="modal fade notification-modal" id="modal4" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	    	<div class="insider">
	      		<div align="center">
	      			<h3>Orden enviada a Producción</h3>
	      			<button class="jugueria-button jugueria-button-sm btn-green" id="concluir">Concluir</button>
	      		</div>
	    	</div>
	    </div>
	  </div>
	</div>

	<div class="modal fade notification-modal" id="modal5" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	    	<div class="insider">
	      		<div align="center">
	      			<h3>Historial de Órdenes</h3>
	      			<table id="ordersHistoryList" class="table table-bordered">
	      				<thead>
	      					<th>No. Orden</th>
	      					<th>Precio</th>
	      					<th>Hora</th>
	      				</thead>
	      				<tbody>
	      				</tbody>
	      			</table>
	      			<button class="jugueria-button jugueria-button-sm btn-green" data-dismiss="modal">Cerrar</button>
	      		</div>
	    	</div>
	    </div>
	  </div>
	</div>

	<div class="modal fade notification-modal" id="modal6" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	    	<div class="insider">
	      		<div align="center">
	      			<h3>Jugo al gusto</h3>
							<label> <input type="checkbox" id="gustoNormal" /> Litro</label>
	      			<textarea id="gustoIngredientes" placeholder="Ingredientes" rows="10" maxlength="500"></textarea>
	      			<button type="button" class="jugueria-button jugueria-button-sm btn-green" id="concluirPedidoAlGusto">Concluir</button>
	      			<button type="button" class="jugueria-button jugueria-button-sm btn-green" data-dismiss="modal">Cerrar</button>
	      		</div>
	    	</div>
	    </div>
	  </div>
	</div>

	<script src="components/js/jquery-3.1.0.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="components/js/index.js"></script>
	<script src="components/js/date.js"></script>
</body>
</html>