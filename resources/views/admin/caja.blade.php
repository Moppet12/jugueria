<div class="row" style="position:relative" align="center">
	<button class="home-btn btn-green">
		<i class="fa-2x fa fa-home"></i>
	</button>
	<button class="return-btn btn-green">
		<i class="fa-2x fa fa-arrow-left"></i>
	</button>
	<div class="modify-box">
		<div class="col-md-6 left-column">
			<div id="summary" class="summary text-md-left">
				<h3 class="text-md-left order"></h3>
				<h4 id="products">
				</h4>
				<hr>
				<h4 id="pricing">
					Cantidad <span class="pull-md-right" id="ammount"></span><br>
					Subtotal <span class="pull-md-right" id="subtotal"></span><br>
					Total <span class="pull-md-right" id="total"></span><br>
				</h4>
			</div>
		</div>
		<div class="col-md-6 right-column">
			<button class="jugueria-button btn-summary btn-blue" id="pagoEfectivo">
				Pago En Efectivo
			</button>
			<button class="jugueria-button btn-summary btn-green" id="pagoTarjeta">
				Pago Con Tarjeta
			</button>
			<button class="jugueria-button btn-summary btn-yellow" id="agregarProductos">
				Agregar Productos
			</button>
			<button class="jugueria-button btn-summary btn-red" id="cancelarOrden">
				Cancelar Orden
			</button>
		</div>	
		<div class="col-md-12 modify-box-footer">
		</div>
	</div>
</div>