<div class="row" style="position:relative;" align="center">
	<button class="home-btn btn-green"><i class="fa-2x fa fa-home"></i></button>
	<button class="return-btn btn-green"><i class="fa-2x fa fa-arrow-left"></i></button>
	<form id="formIngredientes" class="modify-box">
		<input type="hidden" name="ingredientesLength" value="{{ count($ingredientes) }}">
		<?php $n = 0 ?>
		@foreach($ingredientes as $ingrediente)
			<div class="col-md-4">
				<div class="ingredient">
					<label>
						<input type="checkbox" name="ingredientes[]" value="{{ $ingrediente->nombre }}" checked="checked"> {{ $ingrediente->nombre }}
					</label>
				</div>
			</div>
		<?php ++$n ?>
		@endforeach
		<div class="col-md-12 modify-box-footer">
			<div class="pull-md-left">
				<button id="agregarIngredientes" class="jugueria-button jugueria-button-lg btn-brown add-ingredients-btn">
					Agregar Ingredientes
				</button>
			</div>
			<div class="pull-md-right">
				<button type="submit" class="jugueria-button jugueria-button-lg btn-green">
					Aceptar Cambios
				</button>
			</div>
		</div>
	</form>
</div>