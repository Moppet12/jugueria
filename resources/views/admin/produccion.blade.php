<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>La Juguería | Dashboard</title>
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<script src="components/js/jquery-3.1.0.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="//js.pusher.com/3.0/pusher.min.js"></script>
	<script>
		Pusher.log = function(msg) {
	        console.log(msg);
	    };
	</script>
	<script src="components/js/produccionIndex.js"></script>
	<script src="components/js/date.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<style>
		html,body{
			height: 100%;
		}

		header{
			min-height: 100px;
			padding: 25px 0;
		}

		.username{
			margin-top: -25px;
		}

		.delivered{
			background-color: #8df4af;
			padding: 5px;
		}

		.in-process{
			background-color: #ccfb9e;
			padding: 5px;
		}

		.stand-by{
			background-color: #ffc0c0;
			padding: 5px;
		}

		.hold{
			background-color: #f4a28d;
			padding: 5px;	
		}

		footer{
			position: absolute;x
			width: 100%;
			padding: 0 25px;
		}

		.date{
			float: right;
		}
	</style>
</head>
<body>
	<header>
		<div class="container">
			<h1 class="text-md-center main-title">Tablero de Producción</h1>
			<div class="user">
				<div class="pull-md-right username">
					Hola {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
					<div class="btn-group">
						<button class="btn btn-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							&#9660;
						</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="./logout"> Cerrar Sesión </a>
						</div>
					</div>
				</div>
				<div class="pull-md-right user-icon">
					
				</div>
			</div>
		</div>
	</header>
	<section class="container" id="dashboardBody">
		<div class="row" align="center">
			<div class="production-box">
				<table class="table production">
					<thead>
						<tr class="bg-primary">
							<td>Orden</td>
							<td>Detalles</td>
							<td>Estatus</td>
							<td>Opciones</td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<footer>
		<p class="sucursal pull-md-left">Sucursal Constitución Centro</p>
	</footer>

	<div class="modal fade notification-modal" id="confirmOrderModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	    	<div class="modal-body">
	      		<h4>Confirmar orden</h4>
			</div>
			<div class="modal-footer">
	      		<button class="btn btn-primary" id="confirmOrderButton">Confirmar</button>
	      		<button class="btn btn-default" data-dismiss="modal">Cancelar</button>
	    	</div>
	    </div>
	  </div>
	</div>	

	<div class="modal fade notification-modal" id="showIngredientsModal" tabindex="-1" role="dialog" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	    	<div class="insider">
	      		<p>Ingredientes</p>
	    	</div>
	    </div>
	  </div>
	</div>

	<script>
	function showNotification(data) {
	    var text = data.text;
	    // TODO: get the text from the event data
	    
	    // TODO: use the text in the notification
	    toastr.success(text, null, {"positionClass": "toast-bottom-left"});
	}

	var pusher = new Pusher('{{env("PUSHER_KEY")}}');

	var channel = pusher.subscribe('notifications');
	// TODO: Subscribe to the channel

	channel.bind('new-notification',function(data){
		var description = "";
		description += parsePlatillos(data);
	  	$(".production tbody").prepend(`<tr id="orderBody`+ data.id +`" class="stand-by"><td>Orden `+data.id+`<br>`+data.created_at+`</td><td class="employee">`+
		  			description +`</td><td class="status">En Espera</td><td class="options">
					<button type="button" class="btn btn-primary btn-block start-order" data-order="`+ data.id +`">
					Comenzar
					</button>
					<button type="button" id="endOrder`+ data.id +`" class="btn btn-primary btn-block end-order" 
					data-toggle="modal" 
					data-target="#confirmOrderModal"
					data-producing="false">
					Concluir orden
					</button>	
					</td></tr>`);
	  	initializeOptionsButtons();
	});
	// TODO: Bind to the event and pass in the notification handler
	</script>
</body>
</html>