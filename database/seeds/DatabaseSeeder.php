<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MenuSeeder::class);
        DB::table('users')->insert([
            'name' => 'Admin',
            'password' => bcrypt('test'),
            'email' => 'daniel12mm@hotmail.com',
            'username' => 'Admin',
            'id_sucursal' => 1,
            'permiso' => 1,
            'puesto' => 1,
            'estado' => 1
            ]);

        $categorias = array([
            'nombre' => 'Especialidades de la Casa'
            ],
            [
            'nombre' => 'Otros jugos nutritivos'
            ],
            [
            'nombre' => 'Licuados Deliciosos'
            ],
            [
            'nombre' => 'Jugos Terapéuticos'
            ],
            [
            'nombre' => 'Ensaladas y Sándwiches'
            ],
            [
            'nombre' => 'Tosti Huates'
            ]);

        foreach ($categorias as $key => $categoria) {
            DB::table('categorias')->insert([
                'nombre' => $categoria['nombre']
            ]);
        }

        $ingredients = array(['nombre' => 'Naranja'
            ],
            ['nombre' => 'Toronja'],
            ['nombre' => 'Betabel'],
            ['nombre' => 'Apio'],
            ['nombre' => 'Nopal'],
            ['nombre' => 'Espinacas'],
            ['nombre' => 'Zanahoria'],
            ['nombre' => 'Manzana'],
            ['nombre' => 'Avena'],
            ['nombre' => 'Chía'],
            ['nombre' => 'Almendra'],
            ['nombre' => 'Nuez'],
            ['nombre' => 'Papaya'],
            ['nombre' => 'Miel'],
            ['nombre' => 'Fresa'],
            ['nombre' => 'Mango'],
            ['nombre' => 'Durazno'],
            ['nombre' => 'Guayaba'],
            ['nombre' => 'Canela'],
            ['nombre' => 'Leche'],
            ['nombre' => 'Yogurt'],
            ['nombre' => 'Azúcar'],
            ['nombre' => 'Clamato'],
            ['nombre' => 'Limón'],
            ['nombre' => 'Pimienta'],
            ['nombre' => 'Plátano'],
            ['nombre' => 'Granola'],
            // Here!
            ['nombre' => 'Lechuga Romana'],
            ['nombre' => 'Pechuga de Pollo'],
            ['nombre' => 'Champiñones'],
            ['nombre' => 'Tomatitos Cherry'],
            ['nombre' => 'Queso'],
            ['nombre' => 'Olivas Negras'],
            ['nombre' => 'Crotones'],
            ['nombre' => 'Aderezo'],
            ['nombre' => 'Arándanos'],
            ['nombre' => 'Pan con Linaza'],
            ['nombre' => 'Aguacate'],
            ['nombre' => 'Tomate'],
            ['nombre' => 'Cebolla Morada'],
            ['nombre' => 'Sandía'],
            ['nombre' => 'Melón'],
            ['nombre' => 'Jícama'],
            ['nombre' => 'Kiwi'],
            ['nombre' => 'Pasas'],
            ['nombre' => 'Cereal'],
            ['nombre' => 'Coco'],
            ['nombre' => 'Agua'],
            ['nombre' => 'Pepino'],
            ['nombre' => 'Perejil'],
            ['nombre' => 'Ajo'],
            ['nombre' => 'Chamoy'],
            ['nombre' => 'Crema'],
            ['nombre' => 'Cacahuates'],
            ['nombre' => 'Rielito'],
            ['nombre' => 'Tostitos'],
            ['nombre' => 'Salsa Maggie'],
            ['nombre' => 'Valentina'],
            ['nombre' => 'Miguelito'],
            ['nombre' => 'Piña']);

        foreach ($ingredients as $key => $ingredient) {
            DB::table('ingredientes')->insert([
                'nombre' => $ingredient['nombre'],
                'estado' => 1,
                'created_at' => Carbon::now()
                ]);
        }

        $ingrediente_platillo = array([
                'ingrediente_id' => '1',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '3',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '5',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '9',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '10',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '11',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '12',
                'platillo_id' => '2'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '3'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '3'
            ],
            [
                'ingrediente_id' => '11',
                'platillo_id' => '3'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '3'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '3'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '17',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '18',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '19',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '21',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '4'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '5'
            ],
            [
                'ingrediente_id' => '23',
                'platillo_id' => '5'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '5'
            ],
            [
                'ingrediente_id' => '25',
                'platillo_id' => '5'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '6'
            ],
            [
                'ingrediente_id' => '27',
                'platillo_id' => '6'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '6'
            ],
            [
                'ingrediente_id' => '12',
                'platillo_id' => '6'
            ],
            [
                'ingrediente_id' => '26',
                'platillo_id' => '6'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '7'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '7'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '7'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '7'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '8'
            ],
            [
                'ingrediente_id' => '42',
                'platillo_id' => '8'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '8'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '8'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '9'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '9'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '9'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '9'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '9'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '10'
            ],
            [
                'ingrediente_id' => '46',
                'platillo_id' => '10'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '10'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '10'
            ],

            [
                'ingrediente_id' => '22',
                'platillo_id' => '11'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '11'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '11'
            ],
            [
                'ingrediente_id' => '41',
                'platillo_id' => '11'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '12'
            ],
            [
                'ingrediente_id' => '22',
                'platillo_id' => '12'
            ],
            [
                'ingrediente_id' => '47',
                'platillo_id' => '12'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '12'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '5',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '13'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '14'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '14'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '14'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '15'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '15'
            ],
            [
                'ingrediente_id' => '50',
                'platillo_id' => '15'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '15'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '15'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '16'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '16'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '16'
            ],

            [
                'ingrediente_id' => '47',
                'platillo_id' => '17'
            ],
            [
                'ingrediente_id' => '18',
                'platillo_id' => '17'
            ],
            [
                'ingrediente_id' => '42',
                'platillo_id' => '17'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '18'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '18'
            ],
            [
                'ingrediente_id' => '26',
                'platillo_id' => '18'
            ],
            [
                'ingrediente_id' => '47',
                'platillo_id' => '19'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '19'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '19'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '19'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '20'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '20'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '20'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '21'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '21'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '21'
            ],
            [
                'ingrediente_id' => '26',
                'platillo_id' => '21'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '22'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '22'
            ],
            [
                'ingrediente_id' => '12',
                'platillo_id' => '22'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '23'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '23'
            ],
            [
                'ingrediente_id' => '12',
                'platillo_id' => '23'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '23'
            ],
            [
                'ingrediente_id' => '20',
                'platillo_id' => '24'
            ],
            [
                'ingrediente_id' => '26',
                'platillo_id' => '24'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '24'
            ],
            [
                'ingrediente_id' => '18',
                'platillo_id' => '25'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '25'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '25'
            ],
            [
                'ingrediente_id' => '9',
                'platillo_id' => '26'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '26'
            ],
            [
                'ingrediente_id' => '26',
                'platillo_id' => '26'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '5',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '27'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '28'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '28'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '28'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '28'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '29'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '29'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '29'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '29'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '29'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '30'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '30'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '30'
            ],
            [
                'ingrediente_id' => '9',
                'platillo_id' => '31'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '31'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '31'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '31'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '32'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '32'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '32'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '32'
            ],
            [
                'ingrediente_id' => '9',
                'platillo_id' => '33'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '33'
            ],
            [
                'ingrediente_id' => '5',
                'platillo_id' => '33'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '33'
            ],
            [
                'ingrediente_id' => '3',
                'platillo_id' => '34'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '34'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '34'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '34'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '34'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '35'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '35'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '35'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '35'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '35'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '36'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '36'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '36'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '36'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '36'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '37'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '37'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '37'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '37'
            ],
            [
                'ingrediente_id' => '2',
                'platillo_id' => '37'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '38'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '38'
            ],
            [
                'ingrediente_id' => '50',
                'platillo_id' => '38'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '38'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '39'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '39'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '39'
            ],
            [
                'ingrediente_id' => '51',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '4',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '3',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '40'
            ],
            [
                'ingrediente_id' => '48',
                'platillo_id' => '41'
            ],
            [
                'ingrediente_id' => '8',
                'platillo_id' => '41'
            ],
            [
                'ingrediente_id' => '50',
                'platillo_id' => '41'
            ],
            [
                'ingrediente_id' => '7',
                'platillo_id' => '41'
            ],
            [
                'ingrediente_id' => '9',
                'platillo_id' => '42'
            ],
            [
                'ingrediente_id' => '3',
                'platillo_id' => '42'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '42'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '42'
            ],
            [
                'ingrediente_id' => '35',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '30',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '34',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '32',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '28',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '29',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '31',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '33',
                'platillo_id' => '43'
            ],
            [
                'ingrediente_id' => '35',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '15',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '12',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '32',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '28',
                'platillo_id' => '44'
            ],
            [
                'ingrediente_id' => '38',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '37',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '32',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '39',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '28',
                'platillo_id' => '45'
            ],
            [
                'ingrediente_id' => '38',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '40',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '6',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '28',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '33',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '37',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '32',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '39',
                'platillo_id' => '46'
            ],
            [
                'ingrediente_id' => '43',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '44',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '16',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '42',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '14',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '1',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '13',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '60',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '41',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '21',
                'platillo_id' => '47'
            ],
            [
                'ingrediente_id' => '52',
                'platillo_id' => '48'
            ],
            [
                'ingrediente_id' => '23',
                'platillo_id' => '48'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '48'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '48'
            ],
            [
                'ingrediente_id' => '39',
                'platillo_id' => '48'
            ],
            [
                'ingrediente_id' => '53',
                'platillo_id' => '49'
            ],
            [
                'ingrediente_id' => '28',
                'platillo_id' => '49'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '49'
            ],
            [
                'ingrediente_id' => '32',
                'platillo_id' => '49'
            ],
            [
                'ingrediente_id' => '39',
                'platillo_id' => '49'
            ],
            [
                'ingrediente_id' => '54',
                'platillo_id' => '50'
            ],
            [
                'ingrediente_id' => '52',
                'platillo_id' => '50'
            ],
            [
                'ingrediente_id' => '23',
                'platillo_id' => '50'
            ],
            [
                'ingrediente_id' => '43',
                'platillo_id' => '50'
            ],
            [
                'ingrediente_id' => '55',
                'platillo_id' => '50'
            ],
            [
                'ingrediente_id' => '54',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '23',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '56',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '57',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '58',
                'platillo_id' => '51'
            ],
            [
                'ingrediente_id' => '54',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '52',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '23',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '43',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '24',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '59',
                'platillo_id' => '52'
            ],
            [
                'ingrediente_id' => '49',
                'platillo_id' => '52'
            ]);

        foreach ($ingrediente_platillo as $key => $ip) {
            DB::table('ingredientes_platillos')->insert([
                'ingrediente_id' => $ip['ingrediente_id'],
                'platillo_id' => $ip['platillo_id'],
                'created_at' => Carbon::now()
                ]);
        }

        DB::table('pedidos')->insert([
            'id_usuario' => 1,
            'codigo_barras' => '123',
            'costo' => 0,
            'estado' => 1,
            'id_platillos' => '1',
            'ingredientes_extra' => '',
            'tipo_pago' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
    }

}
