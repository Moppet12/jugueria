<?php

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$platillos_1 = array([
    		'nombre' => 'Al Gusto',
            'precio' => 35,
            'id_categoria' => 0,
            'tipo' => 1
            ],
            [
            'nombre' => 'Bengie\'s Power',
    		'precio' => 35,
    		'id_categoria' => 1,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Lily\'s Delight',
    		'precio' => 35,
    		'id_categoria' => 1,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Cristy\'s Special',
    		'precio' => 35,
    		'id_categoria' => 1,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Orson\'s Pro',
    		'precio' => 35,
    		'id_categoria' => 1,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Criessel\'s Precious',
    		'precio' => 35,
    		'id_categoria' => 1,
    		'tipo' => 1
    		]);

    	$platillos_2 = array([
    		'nombre' => 'Buenos Días',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Paraíso',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Sunset',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Amanecer',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Sunny',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Piña Colada',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Verde',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
    		,
    		[
    		'nombre' => 'Atardecer',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 1
    		]
            ,
            [
            'nombre' => 'Especial',
            'precio' => 35,
            'id_categoria' => 2,
            'tipo' => 0
            ]
    		,
    		[
    		'nombre' => 'Solecito',
    		'precio' => 35,
    		'id_categoria' => 2,
    		'tipo' => 0
    		]);

    	$platillos_3 = array([
    		'nombre' => 'Tropical',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Tony\'s',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Bahía',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Jungla',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Charlie',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Pensamiento',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Pancho\'s',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Sunshine',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Brisa',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Despertar',
    		'precio' => 35,
    		'id_categoria' => 3,
    		'tipo' => 1
    		]);

    	$platillos_4 = array([
    		'nombre' => 'Verde o Quema Grasa',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Antienvejecimiento',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Digestivo',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Ácido Úrico',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Vitamínico',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Revitalizante',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Diabetes',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Colesterol',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Gastritis',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Desintoxicante',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Celulitis',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Nutrir la piel',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Desparasitar',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Presión Arterial',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Riñones e Hígado',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		],
    		[
    		'nombre' => 'Triglicéridos',
    		'precio' => 35,
    		'id_categoria' => 4,
    		'tipo' => 1
    		]);

    	$platillos_5 = array([
    		'nombre' => 'Ensaladas Cesar\'s',
    		'precio' => 50,
    		'id_categoria' => 5,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Ensalada de espinacas y fresas',
    		'precio' => 50,
    		'id_categoria' => 5,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Sándwich Sencillo',
    		'precio' => 50,
    		'id_categoria' => 5,
    		'tipo' => 2
    		],
    		[
    		'nombre' => 'Sándwich Completo',
    		'precio' => 50,
    		'id_categoria' => 5,
    		'tipo' => 2
    		],
    		[
    		'nombre' => 'Cocktail de Frutas',
    		'precio' => 50,
    		'id_categoria' => 5,
    		'tipo' => 0
    		]);

    	$platillos_6 = array([
    		'nombre' => 'Tostitos Preparados',
    		'precio' => 50,
    		'id_categoria' => 6,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Tosti-verduras',
    		'precio' => 50,
    		'id_categoria' => 6,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Tostilocos',
    		'precio' => 50,
    		'id_categoria' => 6,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Pepihuates',
    		'precio' => 50,
    		'id_categoria' => 6,
    		'tipo' => 0
    		],
    		[
    		'nombre' => 'Baby-huates',
    		'precio' => 50,
    		'id_categoria' => 6,
    		'tipo' => 0
    		]);

    	$platillos = array($platillos_1, $platillos_2, $platillos_3, $platillos_4, $platillos_5, $platillos_6);

    	foreach ($platillos as $key => $platillo) {
    		foreach ($platillo as $key => $p) {
    			DB::table('platillos')->insert([
    				'nombre' => $p['nombre'],
    				'precio' => $p['precio'],
    				'id_categoria' => $p['id_categoria'],
    				'tipo' => $p['tipo']
    				]);
    		}
    	}
    }
}