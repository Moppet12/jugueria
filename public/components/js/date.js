$(document).ready(function(){
	$("#date").text(getDateText());
});

function getDateText(){
	var date = new Date();
	var day, month;

	switch(date.getDay()){
		case 0: day = "Domingo"; break;
		case 1: day = "Lunes"; break;
		case 2: day = "Martes"; break;
		case 3: day = "Miércoles"; break;
		case 4: day = "Jueves"; break;
		case 5: day = "Viernes"; break;
		case 6: day = "Sábado"; break;
	}

	switch(date.getMonth()){
		case 0: month = "Enero"; break;
		case 1: month = "Febrero"; break;
		case 2: month = "Marzo"; break;
		case 3: month = "Abril"; break;
		case 4: month = "Mayo"; break;
		case 5: month = "Junio"; break;
		case 6: month = "Julio"; break;
		case 7: month = "Agosto"; break;
		case 8: month = "Septiembre"; break;
		case 9: month = "Octubre"; break;
		case 10: month = "Noviembre"; break;
		default: month = "Diciembre"; break;
	}

	return day + ' ' + date.getDate() + ' de ' + month + ' ' + date.getHours() + ':' + date.getMinutes();
}