var UsersData = [];
var ProductsData = [];
var productsTable = "#productsList";
var usersTable = "#usersList";

var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
var lineChartCanvas = $("#lineChart").get(0).getContext("2d");

var areaChartOptions = {
	//Boolean - If we should show the scale at all
	showScale: true,
	//Boolean - Whether grid lines are shown across the chart
	scaleShowGridLines: false,
	//String - Colour of the grid lines
	scaleGridLineColor: "rgba(0,0,0,.05)",
	//Number - Width of the grid lines
	scaleGridLineWidth: 1,
	//Boolean - Whether to show horizontal lines (except X axis)
	scaleShowHorizontalLines: true,
	//Boolean - Whether to show vertical lines (except Y axis)
	scaleShowVerticalLines: true,
	//Boolean - Whether the line is curved between points
	bezierCurve: true,
	//Number - Tension of the bezier curve between points
	bezierCurveTension: 0.3,
	//Boolean - Whether to show a dot for each point
	pointDot: false,
	//Number - Radius of each point dot in pixels
	pointDotRadius: 4,
	//Number - Pixel width of point dot stroke
	pointDotStrokeWidth: 1,
	//Number - amount extra to add to the radius to cater for hit detection outside the drawn point
	pointHitDetectionRadius: 20,
	//Boolean - Whether to show a stroke for datasets
	datasetStroke: true,
	//Number - Pixel width of dataset stroke
	datasetStrokeWidth: 2,
	//Boolean - Whether to fill the dataset with a color
	datasetFill: true,
	//String - A legend template
	legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
	//Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	maintainAspectRatio: true,
	//Boolean - whether to make the chart responsive to window resizing
	responsive: true
};

var barChartOptions = {
	//Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
	scaleBeginAtZero: true,
	//Boolean - Whether grid lines are shown across the chart
	scaleShowGridLines: true,
	//String - Colour of the grid lines
	scaleGridLineColor: "rgba(0,0,0,.05)",
	//Number - Width of the grid lines
	scaleGridLineWidth: 1,
	//Boolean - Whether to show horizontal lines (except X axis)
	scaleShowHorizontalLines: true,
	//Boolean - Whether to show vertical lines (except Y axis)
	scaleShowVerticalLines: true,
	//Boolean - If there is a stroke on each bar
	barShowStroke: true,
	//Number - Pixel width of the bar stroke
	barStrokeWidth: 2,
	//Number - Spacing between each of the X value sets
	barValueSpacing: 5,
	//Number - Spacing between data sets within X values
	barDatasetSpacing: 1,
	//String - A legend template
	legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
	//Boolean - whether to make the chart responsive
	responsive: true,
	maintainAspectRatio: true
};


$(document).ready(function () {
	$(".alert").hide();

	$.ajax({ url: 'admin/day-sales' }).done(function (response) {
		var sales = response.d[0];
		$("#totalSales").text(sales.total == null ? "$" + 0 : "$" + sales.total);
		$("#cashSales").text(sales.cash == null ? "$" + 0 : "$" + sales.cash);
		$("#cardSales").text(sales.card == null ? "$" + 0 : "$" + sales.card);
	})

	$.ajax({ url: 'admin/weekly-sales' }).done(function (response) {
		// Get context with jQuery - using jQuery's .get() method.
		// This will get the first returned node in the jQuery collection.
		var areaChart = new Chart(areaChartCanvas);

		var days = [];
		var sales = [];
		var platillos = [];

		response.forEach(function (day, index) {
			days.push(day.dayname + ' ' + day.day);
			sales.push(day.sales);
			platillos.push(day.platillos);
		});

		//Create the line chart
		areaChart.Line(BuildChartData(days, platillos, sales), areaChartOptions);
	});

	$.ajax({ url: 'admin/monthly-sales' }).done(function (response) {
		var lineChart = new Chart(lineChartCanvas);
		var lineChartOptions = areaChartOptions;
		var months = ["junio"];
		var sales = [0];
		var platillos = [0];
		response.forEach(function (month) {
			months.push(month.monthname);
			sales.push(month.sales);
			platillos.push(month.platillos);
		});
		lineChartOptions.datasetFill = false;
		lineChart.Line(BuildChartData(months, platillos, sales), lineChartOptions);
	});

	$.ajax({ url: 'admin/most-sell-products' }).done(function (response) {
		var name = [];
		var ventas = [];
		response.forEach(function (element) {
			name.push(element.nombre);
			ventas.push(element.ventas);
		});
		var barChartCanvas = $("#barChart").get(0).getContext("2d");
		var barChart = new Chart(barChartCanvas);
		var barChartData = BuildChartData(name, ventas, ventas);
		barChartData.datasets[1].fillColor = "#00a65a";
		barChartData.datasets[1].strokeColor = "#00a65a";
		barChartData.datasets[1].pointColor = "#00a65a";

		barChartOptions.datasetFill = false;
		barChart.Bar(barChartData, barChartOptions);
	});



	/* Products */
	$.ajax({
		url: 'admin/read-platillo',
		type: 'POST'
	})
		.done(function (data) {
			ProductsData = data.d;
			ProductsData.forEach(function (element, index) {
				fillProductsTable(element, index);
			});
			initTable(productsTable);
		})

	$.ajax({
		url: 'admin/read-categoria',
		type: 'POST'
	})
		.done(function (data) {
			data.d.forEach(function (element, index) {
				selected = index == 0 ? true : false;
				$("#categoria").append(`<option ` + selected + ` value="` + element.id + `">` + element.nombre + `</option>`);
			});
		})
		.fail(function () {
			console.log("error");
		});

	/* Users */

	$.ajax({
		url: 'admin/read-users',
		type: 'POST'
	})
		.done(function (data) {
			UsersData = data.d;
			UsersData.forEach(function (element, index) {
				fillUsersTable(element, index);
			});
			initTable(usersTable);
		})
});

function BuildChartData(labels, dataset1, dataset2) {
	return {
		labels: labels,
		datasets: [
			{
				label: "Productos Vendidos",
				fillColor: "rgba(79, 152, 195, 1)",
				strokeColor: "rgba(79, 152, 195, 1)",
				pointColor: "rgba(79, 152, 195, 1)",
				pointStrokeColor: "#c1c7d1",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: dataset1
			},
			{
				label: "Ventas",
				fillColor: "rgba(60,141,188,0.9)",
				strokeColor: "rgba(60,141,188,0.8)",
				pointColor: "#3b8bba",
				pointStrokeColor: "rgba(60,141,188,1)",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(60,141,188,1)",
				data: dataset2
			}
		]
	};
}


function fillProductsTable(element, index) {
	$(productsTable).find('tbody')
		.append(`<tr>
		<td>`+ element.nombre + `</td>
		<td>`+ element.precio + `</td>
		<td>`+ element.nombre_categoria + `</td>
		<td align="center">
		<button class="btn btn-sm btn-primary" onClick="editProduct(`+ index + `,'product')"><i class="fa fa-edit"></i></button>
		<button class="btn btn-sm btn-danger" onClick="deleteProduct(`+ index + `)"><i class="fa fa-times"></i></button>
		</td>
		</tr>`);
}

function fillUsersTable(element, index) {
	$(usersTable).find('tbody')
		.append(`<tr>
		<td>`+ element.name + `</td>
		<td>`+ element.username + `</td>
		<td>`+ element.puesto + `</td>
		<td align="center">
		<button class="btn btn-sm btn-primary" onClick="editUser(`+ index + `,'user')"><i class="fa fa-edit"></i></button>
		<button class="btn btn-sm btn-danger" onClick="deleteUser(`+ index + `)"><i class="fa fa-times"></i></button>
		</td>
		</tr>`);
}

function refreshDataTable(data, selector, table) {
	$(selector).DataTable().clear().destroy();
	data.forEach(function (element, index) {
		table == 0 ?
			fillProductsTable(element, index) :
			fillUsersTable(element, index);
	});
	initTable(selector);
}

function editProduct(index, selector) {
	$("#" + selector + "Title").text("Editar Producto: " + ProductsData[index].nombre);
	$("#" + selector + "EditAlert").fadeIn('fast');
	var selector = $("#productForm");
	selector.find("input[type='hidden']").remove();
	selector.find('[name="nombre"]').val(ProductsData[index].nombre);
	selector.find('[name="precio"]').val(ProductsData[index].precio);
	selector.find('[name="id_categoria"]').val(ProductsData[index].id_categoria);
	selector.prepend('<input name="id" type="hidden" value="' + ProductsData[index].id + '">');
	selector.find('button').html(`<i class="fa fa-btn fa-cart-plus"></i> Editar`);
	selector.attr('onsubmit', "submitForm('productForm','update-platillo')");
}

function editUser(index, selector) {
	$("#" + selector + "Title").text("Editar Usuario: " + UsersData[index].name);
	$("#" + selector + "EditAlert").fadeIn('fast');
	var selector = $("#userForm");
	selector.find("input[type='hidden']").remove();
	selector.find('[name="name"]').val(UsersData[index].name);
	selector.find('[name="email"]').val(UsersData[index].email);
	selector.find('[name="username"]').val(UsersData[index].username);
	selector.find('[name="permiso"]').prop('checked', UsersData[index].permiso == 1 ? true : false);
	selector.find('[name="puesto"]').val(UsersData[index].puesto);
	selector.prepend('<input name="id" type="hidden" value="' + UsersData[index].id + '">');
	selector.find('button').html(`<i class="fa fa-btn fa-user"></i> Editar`);
	selector.attr('onsubmit', "submitForm('userForm','update-user')");
}

function stopEdit(section, selector) {
	var icon;
	var action;
	if (selector == 'product') {
		icon = 'cart-plus';
		action = 'create-platillo';
	}
	else {
		icon = 'user';
		action = 'create-user';
	};
	$("[name='form-" + selector + "']").attr('onsubmit', "submitForm('productForm','update-user')");
	$("#" + selector + "Title").text("Registrar " + section);
	$("#" + selector + "EditAlert").fadeOut('fast');
	$("form").find("button").html(`<i class="fa fa-btn fa-` + icon + `"></i> Registrar`);
	$("form").each(function (index, el) {
		el.reset();
	});
}

function deleteProduct(index) {
	$.post('admin/delete-platillo', { id: ProductsData[index].id }, function (data) {
		if (data.r == 'OK') {
			ProductsData.splice(index, 1);
			refreshDataTable(ProductsData, productsTable, 1);
		}
	});
}

function deleteUser(index) {
	$.post('admin/delete-user', { id: UsersData[index].id }, function (data) {
		if (data.r == 'OK') {
			UsersData.splice(index, 1);
			refreshDataTable(UsersData, usersTable);
		}
	});
}

function submitForm(selector, action) {
	event.preventDefault();
	var form = $("#" + selector);
	$.post('admin/' + action, form.serialize(), function (data) {
		data = JSON.parse(data);
		var alertClass;
		form.find(".alert").fadeIn(('slow'), function () {
			if (data.r == 'OK') {
				alertClass = 'alert-success';
				form[0].reset();
			}
			else
				alertClass = 'alert-danger';
			$(this).addClass(alertClass);
			$(this).text(data.m);
		});

		if (Object.keys(data.d).length > 0) {
			if (action.split('-')[1] == 'user') {
				UsersData.push(data.d);
				refreshDataTable(UsersData, usersTable);
			}
			else {
				ProductsData.push(data.d);
				refreshDataTable(ProductsData, productsTable);
			}
		}

		setTimeout(function () {
			form.find(".alert").fadeOut(('slow'), function () {
				$(this).removeClass(alertClass);
				$(this).text('');
			});
		}, 2000);

	}, 'html');
}

function initTable(table) {
	$(table).DataTable({
		"paging": true,
		"lengthChange": false,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"language": {
			"lengthMenu": "Mostrar _MENU_ entradas por pÃ¡gina",
			"emptyTable": "Tabla sin datos",
			"loadingRecords": "Cargando...",
			"search": "<i class='fa fa-fw fa-search'></i> Buscar:",
			"info": "Mostrando _START_ de _END_ para _TOTAL_ entradas",
			"infoFiltered": "(Filtrado de _MAX_ total de entradas)",
			"infoEmpty": "Mostrando 0 de 0 entradas",
			"paginate": {
				"first": "Primera",
				"last": "Ãšltima",
				"next": "Siguiente",
				"previous": "Anterior"
			}
		}
	});
}