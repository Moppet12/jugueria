var orderSelected;
$(document).ready(function () {
	$.post('admin/read-pedido').done(function (data) {
		var table_body = $('.production tbody');
		var status = {};
		if (data.d)
			data.d.forEach(function (e, index) {
				switch (e.estado) {
					case 1: status.class = 'stand-by';
						status.text = 'En Espera';
						break;
					case 2: status.class = 'delivered';
						status.text = 'Entregado';
						break;
					default: status.class = 'in-process';
						status.text = 'En Proceso';
						break;
				}
				var platillos = "";
				platillos = parsePlatillos(e, index);
				table_body.append(`<tr id="orderBody` + e.id + `" class="` + status.class + `">
					<td>Orden `+ e.id + `<br>` + e.created_at + `</td>
					<td>`+ platillos + `</td>
					<td class="status">`+ status.text + `</td>
					<td class="options">
						<button type="button" class="btn btn-primary btn-block start-order" data-order="`+ e.id + `">
						Comenzar
						</button>
						<button type="button" id="endOrder`+ e.id + `" class="btn btn-primary btn-block end-order"
						 data-toggle="modal"
						 data-target="#confirmOrderModal" 
						 data-producing="false">
						Concluir orden
						</button>
					</td>
					</tr>`);
			});

		initializeOptionsButtons();
	});

	$("#confirmOrderButton").on('click', function (event) {
		event.preventDefault();
		//Submit order
		$.post("admin/update-pedido", { id: orderSelected, estado: 2 })
			.done(function (response) {
				if (response.r == 'OK') {
					$("#orderBody" + orderSelected).remove();
				}
				else {
					alert(response.m);
				}
			});

		$("#confirmOrderModal").modal('toggle');
	});
});

function initializeOptionsButtons() {
	$("button[id^='endOrder']:not([data-producing='true'])").hide();

	$(".start-order").click(function () {
		$(this).each(function (index, el) {
			if ($(this)[0].dataset.order == el.dataset.order) {
				$("#orderBody" + el.dataset.order).toggleClass('stand-by in-process');
				$("#orderBody" + el.dataset.order + " td.status").text('En Proceso');
				$(this).siblings()[0].dataset.producing = true;
				orderSelected = parseInt(el.dataset.order);

				$(this).fadeOut('fast', function () {
					$("#endOrder" + el.dataset.order + "").fadeIn('fast');
				});
			}
		});
	});
}

function parsePlatillos(object) {
	var platillos = "";
	var ingredientes_platillo = object.ingredientes_extra.split(',');
	var op;

	try {
		object.platillos.split(',').forEach(function (platillo, index) {
			var type = parseInt(object.normal.split(',')[index]) == 1 ? 'Normal' : 'Litro';
			var ingredientes = ingredientes_platillo[index].split('|');
			platillos += platillo + ': <small>' + type + '</small> <br> <small>' + ingredientes.join(', ') + '</small> <br> ';
		});
	}
	catch (exc) {
		object.platillos.forEach(function (platillo, index) {
			var type = parseInt(platillo.pivot.normal) == 1 ? 'Normal' : 'Litro';
			var ingredientes = ingredientes_platillo[index].split('|');
			platillos += platillo.nombre + ': <small>' + type + '</small> <br> <small>' + ingredientes.join(', ') + '</small> <br> ';
		});;
	}
	return platillos;
}