// Global Variables

var body = $('#dashboardBody');
var order = {};
var platillos = {};
var jMenu = ' .jugueria-menu';
var d;
var titleSelector = $("header .main-title");
var mainTitle = {};
var petitioner;
var np = 0;
var parentId;
var notificationModal = $("#modal1");
var cajaModal = $("#modal2");
var modalDefaultOptions = { backdrop: 'static', keyboard: false };
var user_id;
var barcode;
//Temp
user_id = 1;
// \.Global Variables

// Onload

$(document).ready(function () {
	mainTitle.home = $("header .main-title").text();
	body.html('<h1 class="text-md-center">Cargando...</h1>');
	obtenerCategorias.done(function (data) {
		order.mostrarCategorias(data);
	});
	$("#caja").click(function () {
		order.mostrarCaja();
	});
	$("#ordenar").click(function () {
		order.ordenarMas();
	});

	$("#concluirPedidoAlGusto").on('click', function () {
		var normal = !$("#gustoNormal").prop('checked');
		var ingredientes = $("#gustoIngredientes").val();
		platillos[np] = {};
		platillos[np].id_platillo = 1;
		platillos[np].nombre_platillo = 'Jugo al gusto';
		platillos[np].precio = 35;
		platillos[np].ingredientes_extra = ingredientes;
		platillos[np].normal = normal;
		np++;
		$("#gustoNormal").prop('checked',false);
		$("#gustoIngredientes").val('');
		$("#goToCart").prop('disabled', false);
		$("#modal6").modal('hide');
	});
});

// \.Onload

// Order Constructors

order.mostrarCategorias = function (data) {
	titleSelector.text(mainTitle.home);
	body.fadeIn(300);
	var disableGoToCart = Object.keys(platillos).length == 0 ? 'disabled' : '';
	body.html('<div class="row"><div class="col-md-6" id="leftBar"><div class="jugueria-menu" align="center"></div></div><div class="col-md-6" id="rightBar">' +
		'<div class="jugueria-menu" align="center"></div></div><div class="row"><div class="col-md-12"><div class="jugueria-menu-footer" align="center">' +
		'<button class="jugueria-button btn-orange" data-toggle="modal" data-target="#modal6">Jugo al gusto</button>' +
		'<button id="showOrdersHistory" class="jugueria-button btn-orange" data-toggle="modal" data-target="#modal5">Ver historial de Órdenes</button>' +
		'<button id="goToCart" class="jugueria-button btn-orange" ' + disableGoToCart + '>Ir a caja</button>' +
		'</div></div></div>');
	d = data.d;
	var bar;
	petitioner = 0;
	d.forEach(test);
	$('.categoria-btn').click(function () {
		order.categoria = $(this).attr('id');
		order.nombre_categoria = $(this).text();
		body.fadeOut(300, function () { body.html(''); body.html('<h1 class="text-md-center">Cargando...</h1>'); order.mostrarPlatillos() });
	});

	$("#goToCart").on('click', function () {
		order.mostrarCaja();
	});

	$("#showOrdersHistory").on('click', function (event) {
		event.preventDefault();
		/* Act on the event */
		$("#ordersHistoryList tbody").html('');
		$.ajax({
			url: 'admin/day-orders',
		}).done(function (data) {
			data.forEach(function (el) {
				$("#ordersHistoryList tbody").append(`<tr>
				<td>`+ el.id + `</td>
				<td>$`+ el.costo + `</td>
				<td>`+ el.created_at.split(' ')[1] + `</td>
				</tr>`);
			});
		});

	});
}

order.mostrarPlatillos = function () {
	mainTitle.categoria = order.nombre_categoria;
	titleSelector.text(mainTitle.categoria);
	body.fadeIn(300);
	$.post('admin/read-platillo-by-categoria', { id: order.categoria }).done(function (data) {
		body.html('');
		body.html('<div class="row dashboard-body"><button class="home-btn btn-green"><i class="fa-2x fa fa-home"></i></button><div class="col-md-6" id="leftBar">' +
			'<div class="jugueria-menu" align="center"></div></div><div class="col-md-6" id="rightBar"><div class="jugueria-menu"></div></div></div>');
		d = data.d;
		var bar;
		petitioner = 1;
		d.forEach(test);
		$(".modify").click(function () {
			parentId = $(this).parent().attr('id');
			parentPrecio = $(this).parent().attr('precio');
			order.nombre_platillo = ($(this).parent().text()).split(' ');
			order.normal = !$(this).parent().find("input[name^='liter']").prop('checked');
			cleanNombrePlatillo();
			body.fadeOut(300, function () { body.html(''); body.html('<h1 class="text-md-center">Cargando...</h1>'); order.mostrarIngredientes(parentId, parentPrecio); })
		});
		$(".cart").click(function () {
			order.nombre_platillo = ($(this).parent().text()).split(' ');
			cleanNombrePlatillo();
			platillos[np] = { 'id_platillo': $(this).parent().attr('id') };
			platillos[np].nombre_platillo = order.nombre_platillo;
			platillos[np].precio = $(this).parent().attr('precio');
			platillos[np].ingredientes_extra = '';
			platillos[np].normal = !$(this).parent().find("input[name^='liter']").prop('checked');
			np++;
			notificationModal.modal(modalDefaultOptions);
			$("#modal1 #product").text(order.nombre_platillo);
		});
		homeListener();
	});
}

order.mostrarIngredientes = function (id, p) {
	mainTitle.ingredientes = 'Modificando ' + order.nombre_platillo;
	titleSelector.text(mainTitle.ingredientes);
	body.fadeIn(300);
	$.post('admin/read-ingrediente', { 'id': id, 'type': 0 }).done(function (data) {
		appendBody(data);
		var except = [];
		$.post('admin/read-ingrediente', { 'id': id, 'type': 1 }).done(function (d) {
			d.forEach(function (e) {
				except.push(e.ingrediente_id);
			});
		});
		$("#agregarIngredientes").click(function (ev) {
			ev.preventDefault();
			var m = $("#modal3");
			m.modal();
			m.find(".insider").html('<h1>Cargando...</h1>');
			$.post('admin/read-more-ingredients', { 'except': except }).done(function (d) {
				var res = $.map(d, function (v) {
					return [v];
				});
				console.log(res);
				m.find('.insider').html('');
				m.find('.insider').append('<div class="row"></div>');
				res.forEach(function (e, i) {
					if (e.estado == 1)
						m.find('.insider .row').append('<div class="col-xs-3 content">' +
							'<p class="text-md-center">' + e.nombre + '<i class="pull-md-right btn-green add-plus" ingredient_id="' + e.id + '" ingredient_name="' + e.nombre + '">+</i></p>' +
							'</div>');
					if (i === res.length - 1) {
						$('.add-plus').click(function (e) {
							console.log('added!');
							appendIngredient($(this).attr('ingredient_name'), $(this).attr('ingredient_name'), $(this), except);
						});
					}
				});
				m.find('.insider .row').append(`
					<div class="col-md-12">
						<input type="text" placeholder="Ingrediente" id="ingredient-name">
						<i class="btn-green add-plus" id="add-ingredient-manual">+</i>
					</div>`);
				$("#add-ingredient-manual").click(function () {
					var ingredient = $("#ingredient-name").val();
					appendIngredient(ingredient, ingredient, null, except);
					$("#ingredient-name").val("");
				});
			});
		});
		$("#formIngredientes").submit(function (ev) {
			ev.preventDefault();
			var ingredientesExtra = '';
			var pipe = '|';
			$('#formIngredientes [name="ingredientes[]"]:checked').each(function (i) {
				if (i == $('#formIngredientes [name="ingredientes[]"]').length - 1)
					pipe = '';
				ingredientesExtra += $(this).val() + pipe;

			});
			var l = $('input[name="ingredientesLength"]').val();
			var ing = {};
			for (var i = 0; i < l; i++) {
				if (i == 0) ing.id_platillo = parentId;
				var e = $("#formIngredientes input[name='ingrediente" + i + "']");
				if (e.prop('checked')) {
					ing[i] = e.val();
				}
			}
			platillos[np] = ing;
			platillos[np].nombre_platillo = order.nombre_platillo;
			platillos[np].precio = p;
			platillos[np].ingredientes_extra = ingredientesExtra;
			platillos[np].normal = order.normal;
			np++;
			notificationModal.modal(modalDefaultOptions);
			$("#modal1 #product").text(order.nombre_platillo);
			// body.fadeOut(300, function(){
			// 	body.html('');
			// 	order.mostrarPlatillos();
			// });
		});
		homeListener();
		returnListener();
	});
}

order.mostrarCaja = function () {
	mainTitle.caja = 'Confirmar Orden';
	titleSelector.text(mainTitle.caja);
	f = function (e) {
		body.fadeOut(300, function () {
			obtenerCaja.done(function (data) {
				body.html(data);
				$.ajax({ url: 'admin/get-current-order' }).done(function (response) {
					$("#summary h3").text("Orden " + response.d.id);
				});

				var platillosLength = Object.keys(platillos).length;
				var printed;
				var subtotal = 0;
				var total = 0;
				for (var i = 0; i <= platillosLength - 1; i++) {
					var counter = 0;
					for (var e = 0; e <= platillosLength - 1; e++) {
						if (platillos[i].id_platillo == platillos[e].id_platillo) {
							counter++;
							platillos[e].printed = counter;
						}
					}
					if (platillos[i].printed == 1)
						$("#summary #products").append(platillos[i].nombre_platillo + '<span class="pull-md-right counter" id="counter' + platillos[i].id_platillo + '">x 1</span><br>');
					else
						$("#summary #products #counter" + platillos[i].id_platillo).text('x ' + counter);
					platillos[i].precio = !platillos[i].normal && parseInt(platillos[i].precio) == 35 ? parseInt(platillos[i].precio) + 25 : parseInt(platillos[i].precio);
					subtotal += platillos[i].precio;
				}
				total = subtotal;
				$("#pricing #ammount").text(platillosLength);
				$("#pricing #subtotal").text('$' + subtotal);
				$("#pricing #total").text('$' + total);
				body.fadeIn(300);
				$("#pagoEfectivo").click(function () {
					order.enviarPago(0);
					order.concluirPedido();
				});
				$("#pagoTarjeta").click(function () {
					order.enviarPago(1);
					order.concluirPedido();
				});
				$("#agregarProductos").click(function () {
					body.fadeOut(300, function () {
						obtenerCategorias.done(function (data) {
							order.mostrarCategorias(data);
						});
					});
				});
				$("#cancelarOrden").click(function () {
					$("#modal2 .insider").html('<span class="standOut">Confirmar cancelación de orden</span>'
						+ '<button class="jugueria-button jugueria-button-sm btn-green" id="return">Regresar</button>'
						+ '<button class="jugueria-button jugueria-button-sm btn-red" id="confirm">Confirmar</button>');
					cajaModal.modal(modalDefaultOptions);
					$("#modal2 #return").click(function () {
						cajaModal.modal('hide');
					});
					$("#modal2 #confirm").click(function () {
						platillos = {};
						np = 0;
						cajaModal.modal('hide');
						body.fadeOut(300, function () {
							obtenerCategorias.done(function (data) {
								order.mostrarCategorias(data);
							});
						});
					});
				});
			});
			homeListener();
			returnListener();
		});
	}
	modalHidden(f);
}

order.ordenarMas = function () {
	f = function (e) {
		body.fadeOut(300, function () {
			obtenerCategorias.done(function (data) {
				order.mostrarCategorias(data);
			});
		});
	}
	modalHidden(f);
}

order.enviarPago = function (type) {
	var data = $.map(platillos, function (v, i) {
		return [v];
	});
	$.ajax({
		type: 'POST', url: 'admin/create-pedido', data: { 'platillos': data, 'pago_type': type, 'id_usuario': user_id }, success: function (data) {
			console.log(data);
		}
	});
}

order.concluirPedido = function () {
	$("#modal4").modal(modalDefaultOptions);
	$("#modal4 #concluir").click(function () {
		platillos = {};
		np = 0;
		$("#modal4").modal('hide');
		body.fadeOut(300, function () {
			obtenerCategorias.done(function (data) {
				order.mostrarCategorias(data);
			});
		});
	});
}

// \.Order Constructors

// Application Functions

var obtenerCategorias = $.post('admin/read-categoria');
var obtenerCaja = $.post('admin/get-caja');

var modify = function () {
	console.log($(this));
}

var test = function (e, i) {
	var specificInfo = "";
	if (e.tipo == 1)
		specificInfo = "<br><label for='liter" + i + "'><small>Litro</samll></label> <input type='checkbox' id='liter" + i + "' name='liter" + i + "'>";
	else if (e.tipo == 2)
		specificInfo = "<br><label for='roast" + i + "'><small>Tostado</small></label> <input type='checkbox' id='roast" + i + "' name='roast" + i + "'>";

	(i % 2 == 0) ? bar = '#leftBar' : bar = '#rightBar';
	switch (petitioner) {
		case 1:
			$(bar + jMenu).append('<div class="button-with-options btn-blue" id="' + e.id + '" precio="' + e.precio + '">' + e.nombre + ' <button class="pull-md-right jugueria-button-sm option btn-yellow modify">' +
				'Modificar</button><button class="pull-md-right jugueria-button-sm option btn-green cart">A Carrito</button>' + specificInfo + '</div>');
			break;
		default:
			$(bar + jMenu).append('<button class="jugueria-button jugueria-button-lg btn-green categoria-btn" id="' + d[i].id + '">' + d[i].nombre + '</button>');
	}
}

function appendBody(data) {
	body.html(data);
}

function appendIngredient(id, name, selector, array) {
	$('.modify-box').append('<div class="col-md-4"><div class="ingredient"><label><input type="checkbox" name="ingredientes[]" value="' +
		id + '" checked="checked"> ' + name + '</label></div></div>')
	selector ? selector.parent().parent().hide() : null;
	array.push(id);
	var ingredientsLengthSelector = $('input[name="ingredientesLength"]');
	var value = parseInt(ingredientsLengthSelector.val());
	ingredientsLengthSelector.val(++value);
}

function cleanNombrePlatillo() {
	var m = (order.nombre_platillo).indexOf('ModificarA');
	(order.nombre_platillo).splice(m, 2);
	var temp = '';
	order.nombre_platillo.forEach(function (e) {
		temp += ' ' + e;
	});
	order.nombre_platillo = temp;
}

function homeListener() {
	$(".home-btn").click(function () {
		body.fadeOut(300, function () {
			obtenerCategorias.done(function (data) {
				order.mostrarCategorias(data);
			});
		});
	});
}

function returnListener() {
	$(".return-btn").click(function () {
		body.fadeOut(300, function () {
			body.html('');
			order.mostrarPlatillos();
		});
	});
}

function modalHidden(f) {
	if (notificationModal.hasClass('in')) {
		notificationModal.modal('hide');
		notificationModal.on('hidden.bs.modal', f);
	}
	else
		f();
}

// \.Applicacions Functions	